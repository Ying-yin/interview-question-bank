###  BFC是什么?什么场景下需要使用?

#### 1. BFC的含义

> ``` 
> BFC：（Block Formatting Context）块级格式化上下文
> 
> 是一块独立的渲染区域（出发了BFC，这块区域就是一块独立的渲染区域） 会将BFC里面的内容和外面的内容隔离	
> ```

#### 2. 触发BFC的方式

``` 
1.position：absolute/fixed
2.float：left/right 浮动的元素多个放在一起，会互相隔开
3.overflow：非visible hidden/auto/scroll
4.display:inline-block
```

#### 3. 实际例子（两端固定， 中间自适应）

> **以浮动为例**：

``` 
```

> 

#### 4.BFC的应用：

```
1.处理块级元素： 上下margin合并的问题，
2.处理margin塌陷
3.清除浮动
4.实现自适应布局
    flex  => display:flex; 左边定宽，右边flex:1;
    浮动 => 左边左浮动，右边有浮动，中间盒子overflow：hidden;
    定位 => 先定位再设置padding即可
```

