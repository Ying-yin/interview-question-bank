### 虚拟DOM

> 用JS去按照DOM结构来实现的树形结构对象，你也可以叫做**DOM对象**

**DOM-diff**的过程：

1. 用JS对象模拟DOM（虚拟DOM）
2. 把此虚拟DOM转成真实DOM并插入页面中（render）
3. 如果有事件发生修改了虚拟DOM，比较两棵虚拟DOM树的差异，得到差异对象（diff）
4. 把差异对象应用到真正的DOM树上（patch）