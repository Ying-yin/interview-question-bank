### vue中为什么要有key

#### key的作用

作为一个`DOM`节点的标识值，结合`Diff算法`可实现对节点的复用。(`key`相同的节点会被复用。) 只有当`key`发生改变时，才会触发节点的重新渲染。否则Vue将会复用之前的节点，通过改变节点的属性来实现节点的更新。

#### key使用id与index的区别

不推荐使用`index`作为`key`，因为这种做法会导致某些节点被错误地原地复用，具体如下：

- 性能损耗：列表渲染时会导致变动项往后的所有列表节点(内容)的更新（相当于`key`没发挥作用）。
- 出现错误：某些节点在错误的位置被复用。(例如当列表项中使用到复选框时)s

##### 性能损耗

> 列表渲染时会导致变动项往后的所有列表节点(内容)的更新（相当于`key`没发挥作用）
>
> 需要注意的是，变动项往后的所有列表节点的更新本质是节点属性的更新，节点本身会被复用。
>
> 
>
> 具体看下面的例子，就是`v-for`渲染一个长度为10000的列表，然后在Vue `mounted` 1s后，执行一个删除列表首项或在列表头插入新项，观察两种`key`绑定的具体页面更新开销。 页面开销使用chrome的performance选项卡来测算 删除列表首项

``` js
<!-- 测试代码 -->
<template>
  <div>
    <div v-for="(item, index) in arr" :key="index 或 item.id">
      {{item.data}}
    </div>
  </div>
</template>

<script>
export default {
  name: 'HelloWorld',
  data(){
    return {
      arr: Array.from({length: 10000}, (v, i) => {return {id: i, data: i}})
    }
  },
  mounted(){
    setTimeout(()=>{
      /* 
      1. this.shiftArr()	// 删除首项
      或
      2. this.unShiftArr()	// 在首部插入新项
      */
    }, 1000)
  },
  methods: {
    shiftArr(){
      this.arr.shift();
    },
    unshiftArr(){
      this.arr.unshift({id: -1, data: -1});
    }
  }
}
</script>

```



##### 出现错误

> 某些节点在错误的位置被复用。(例如当列表项中使用到复选框时)

``` js
<!-- 测试代码 -->
<template>
  <div>
    <button @click="test">删除列表第一项</button>
    <div v-for="(item, index) in arr" :key="index 或 item.id">
      <input type="checkbox" />
      {{item.data}}
    </div>
  </div>
</template>

<script>
export default {
  name: 'HelloWorld',
  data(){
    return {
      arr: Array.from({length: 5}, (v, i) => {return {id: i, data: i}})
    }
  },
  methods: {
    test(){
      this.arr.shift();
    }
  }
}
</script>

```

