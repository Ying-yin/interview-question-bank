# 面向对象编程（OOP）

> 简单来说，面向对象编程就是将你的需求抽象成一个对象，然后对这个对象进行分析，为其添加对应的特征（属性）与行为（方法），我们将这个对象称之为 **类**。

## 面向对象的特点

#### 1.封装

- 创建一个类

> 在 `javascript` 中要创建一个类是很容易的，比较常见的方式就是首先声明一个函数保存在一个变量中（一般类名首字母大写），然后将这个函数（类）的内部通过对 `this` 对象添加属性或者方法来实现对类进行属性或方法的添加，

```js
//创建一个类
var Person = function (name, age ) {
	this.name = name;
	this.age = age;
}
```

> 我们也可以在类的原型对象(`prototype`)上添加属性和方法，有两种方式，一种是一一为原型对象的属性赋值，以一种是将一个对象赋值给类的原型对象：

```js
//为类的原型对象属性赋值
Person.prototype.showInfo = function () {
    //展示信息
    console.log('My name is ' + this.name , ', I\'m ' + this.age + ' years old!');
}

//将对象赋值给类的原型对象
Person.prototype = {
    showInfo : function () {
	    //展示信息
	    console.log('My name is ' + this.name , ', I\'m ' + this.age + ' years old!');
	}
}
```

> 当我们要用的时候，首先得需要使用 `new` 关键字来实例化（创建）新的对象，通过 `.` 操作符就可以使用实例化对象的属性或者方法了~

```js
var person = new Person('Tom',24);
console.log(person.name)        // Tom
console.log(person.showInfo())  // My name is Tom , I'm 24 years old!
```

- 属性与方法的封装

在大部分面向对象的语言中，经常会对一些类的属性和方法进行隐藏和暴露，所以就会有 私有属性、私有方法、公有属性、公有方法等这些概念~