### Flex

> 布局的传统解决方案是基于盒状模型，依赖 `display` + `position` + `float` 方式来实现，灵活性较差。
>
> 2009年，W3C提出了一种新的方案-Flex，Flex是Flexible Box的缩写，意为”弹性布局”。Flex可以简便、完整、响应式地实现多种页面布局。



#### 1. 基本概念:

```
采用 Flex 布局的元素，称为 Flex 容器（flex container），简称"容器"。它的所有子元素自动成为容器成员，称为 Flex 项目（flex item），简称"项目"。
```

```
容器默认存在两根轴：主轴、侧轴，Flex项目默认沿主轴排列
```

#### 2. 基本语法：

``` css
.box {
  display: flex; /* 或者 inline-flex */
}
```

#### 3.属性

##### 3.1 设置主轴方向：flex-direction

```css
语法
.box {
    flex-direction: row | row-reverse | column | column-reverse;
}

row 表示从左向右排列
row-reverse 表示从右向左排列
column 表示从上向下排列
column-reverse 表示从下向上排列
```

##### 3.2 设置换行： flex-wrap

```css
.box {
    flex-wrap: nowrap | wrap | wrap-reverse;
}

nowrap(缺省)：所有Flex项目单行排列
wrap：所有Flex项目多行排列，按从上到下的顺序
wrap-reverse：所有Flex项目多行排列，按从下到上的顺序

```

##### 3.3  设置主轴对齐方式：`justify-content`

```css
.box  {
    justify-content: flex-start | flex-end | center | space-between | space-around | space-evenly;
}
flex-start(缺省)：从启点线开始顺序排列
flex-end：相对终点线顺序排列
center：居中排列
space-between：元素分布在两边 再平分剩余空间
space-around：是直接平分剩余空间
space-evenly：项目均匀分布，所有项目之间及项目与边框之间距离相等

作者：毛瑞_三十课
链接：https://juejin.cn/post/6844903586841755655
来源：稀土掘金
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
```

##### 3.4 设置侧轴对齐方式：`align-items`

```cdd
.box {
  align-items: stretch | flex-start | flex-end | center | baseline;
}
stretch(缺省)：交叉轴方向拉伸显示
flex-start：项目按交叉轴起点线对齐
flex-end：项目按交叉轴终点线对齐
center：交叉轴方向项目中间对齐
baseline：交叉轴方向按第一行文字基线对齐
```





