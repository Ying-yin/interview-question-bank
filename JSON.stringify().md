## 深拷贝

> 深拷贝：在堆内存中重新开辟一个存储空间，完全克隆一个一模一样的对象 
>
> 浅拷贝：不在堆内存中重新开辟空间，只复制栈内存中的引用地址。本质上两个对象(数组）依然指向同一块存储空间

### 深拷贝的方式

* 递归方式(推荐，项目中最安全最常用)
* JSON.stringify() (不推荐，有坑)
* 第三方库lodash的cloneDeep()（就情况而定，如果项目中原先就有lodash这个第三方库，可以使用，否则还是推荐使用递归函数。不然成本太高。）
* JQuery的extend()函数 (推荐在JQuery项目中使用，其他项目依然推荐是用递归函数)



### JSON.stringify() 深拷贝的问题

对象中有时间类型的时候，序列化之后会变成字符串类型。

对象中有undefined和Function类型数据的时候，序列化之后会直接丢失。

对象中有NaN、Infinity和-Infinity的时候，序列化之后会显示null。

对象循环引用的时候，会直接报错。





### 怎么解决?

1. 深拷贝建议使用递归，安全方便。
2. 使用第三方库lodash中得cloneDeep()方法; 但是如果我们的项目中只需要一个深拷贝的功能，这种情况下为了一个功能引入整个第三方库就显得很不值得了。不如写一个递归函数对于项目来说性能更好。
3. JQ的extend()方法进行深拷贝，推荐在JQ中使用






