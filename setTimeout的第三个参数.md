```js
for (var i = 0; i < 10; i++) {
      setTimeout(() => {
        console.log(i);
      }, 0);
   }
// 思考输出的结果及为什么是这个结果

// 这个结果很容易知道 for循环要等异步执行完才执行 所以输出10次10
```

## 1.如果把var 改成let 会输出什么 为什么

改成let 会依次输出0~9 将全局作用于变成块级作用域 把作用域限制住

## 2.如何改代码让其输出0~9

1).将作用域变成块级 即用let替代var

```js
for (let i = 0; i < 10; i++) {
      setTimeout(() => {
        console.log(i);
      }, 0);
   }
```

2).在全局作用域内使用闭包和立即执行函数(本人不喜欢闭包)

```js
for (var i = 0; i < 10; i++) {
        (function (j) {
          setTimeout(() => {
            console.log(j);
          }, 0);
      })(i);
   }
复制代码
```

3).用 setTimeout 的第三个参数解决

```css
for (var i = 0; i < 10; i++) {
        setTimeout((j) => {
          console.log(j);
      }, 0, i);
   }
复制代码
```

setTimeout第三个参数指向的就是函数的参数

